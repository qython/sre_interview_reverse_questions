# ADMIN/SRE/DEVOPS - PYTANIA DO PRACODAWCY

Wychodzę z założenia że rozmowa kwalifikacyjna nie jest tylko po to aby poracodawca ocenił przydatność kandydata w swojej firmie, ale również aby kandydat przekonał się czy chce tam pracować. To już nie lata 80-ty czy 90-te gdzie pracownik był na łasce pracodawców. Żyjemy w czasach kiedy praca jest transakcją wiązaną. My pracujemy, firma nas szanuje i płaci za naszą robotę.

Stworzyłem ten dokument aby pomóc pracownikowi zweryfikować czy firma, do której aplikuje będzie mu odpowiadać.
Zakładam że jesteś ogarniętym człowiekiem i wiesz gdzie startujesz. Nie znajdziesz tutaj pytań, które są nie poważne np. czym zajmuje się Państwa firma. 

**Proszę abyś nie zadawał wszystkich pytań**, przeczytaj wszystkie przed rozmową i wbierz te, które pasują do stanowiska na jakie aplikujesz. Pomiń pytania na które możesz odpowiedzieć sam (znajomi, internet, strona pracodawcy czy HRy).

## Ogólnie pytania o firmie
 * Jak wygląda struktura organizacyjna w firmie. Proszę umiejscowić zespół w którym będę pracował w strukturze ?
 * Ilu managerów jest od mojego stanowiska do CEO (płaska struktura vs bardzo rozbudowana) ?
 * Jaka jest rola polskiego oddziału firmy (tickeciarnia, wyrobnictwo, równouprawnienie, podejmowanie kluczowych decyzji) ?
 * Gdzie zapadają kluczowe decyzje w firmie i czy mam/mamy na nie wpływ ? 
 * Jak wygląda załatwianie codziennych miekkich spraw: dostępy, sprzęt, kontakt z HR (tickety/telefon/maile/face2face) ?
 * Jak wygląda komunikacja w firmie (slack, jabber, maile, telefon, 13 komunikatorów, irc) ? 
 * Czy stosujecie dresscode w praktyce ?
 * Jak wygląda **realnie** ścieżka awansu, oceny pracownika, bunusy ?
 * Jak wyglądaja **realnie** sprawa szkoleń i rozwoju ?
 * Czy ludzie w firmie są otwarci na pomysły, hackatony, uprawianie sportów, aktywności po za pracą ?
 * Czy firma wspiera uprawianie sportu przez pracowników (np. dofinansowanie do sali czy pakietów startowych)
 * Jaki jest podstawowy system operacyjny używany w firmie, dlaczego Windows i czy można zainstalować Linux ?
 * Jakiego sprzętu używamy w pracy (laptop,pc,telefon służbowy) ?
 * Praca z domu czy z biura ? 
 * W jakich godzinach pracujemy i czy są on-calle ? 
 * Podejście firmy do OpenSource/FreeSoftware/EnterYourPrice ? 
 * Czy można zmienać zespoły i jak jest to rozwiązane ?

## Zespół
 * Jak wygląda typowy dzień i organizacja pracy, podział zadań, rozliczalność ?
 * Jaki jest wpływ zespołu na rzeczywistość w firmie (czy jesteśmy decyzyjni, szanowani, liczą się z naszym zdaniem) ? 
 * Ile osób jest w moim przyszłym zespole + przekrój: junior, regular, senior ? 
 * 3 najpopularniejsze narzędzia, których używa zespół ?
 * Czy w innych oddziałach firmy są zespoły, które robią to samo ? 
 * Czy pracujemy z ludzmi w innych strefach czasowych ? 
 * Jak często i jak długie są spotkania zespołowe ?
 * Jak powiem że chcę zainstalować opensource toola rozwiązującego jakiś problem to zostanę wyśmiany bo to nie EnterYourPrice ? 

## Ja
 * Czym będę się dokładnie zajmował + stack technologiczny ? 
 * Jaki będzie mój wpływ na rzeczywistość w firmie ? 
 * Czy moja praca będzie powtarzalna, nudna, monotonna, ticketowana ? 
 * Określ w % ile czasu zajmuje techniczna praca vs nietechniczna (hr, papierkologia, consulting, kasowanie głupich maili z skrzynki)
 * Co mam rozumiesz przez techniczną pracę (konsola, kodowanie, excel, powerpoint, spotkania) ? 
 * Czy będę miał roota i swoje maszynki ?
 * Kiedy realnie zacznę pisać: kod/skrypty/patche ? 
 * Ile czasu będę spędzał na spotkaniach, rozmowach, pierdołach vs faktyczna techniczna praca ? 
 * Czy będę miał admina na mojej maszynie służbowej ?
 * Czy będę mógł instalować oprogramowanie na mojej maszynie służbowej ?
 * Czy będę musiał wyjeżdżać za granicę i jak często ?
 * Jak długo trwa okres próbny i proces jego oceny ?
